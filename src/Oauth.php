<?php


namespace Int\Services\Client;


class Oauth extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.oauth/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'oauth';


    /**
     * List Users
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listUsers(array $data = [], array $headers = []): array
    {
        return $this->get('users', $this->dataFormatJson($data), $headers);
    }

    /**
     * Sign up
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function signup(array $data = [], array $headers = []): array
    {
        return $this->post('signup', $this->dataFormatFormParams($data), $headers);
    }

    /**
     * Me
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function me(array $data = [], array $headers = []): array
    {
        return $this->get('me', $this->dataFormatFormParams($data), $headers);
    }

    /**
     * Get User By Id
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function getUserById(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('users/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Update by user id
     *
     * @param  string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateByUserId(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('users/' . $id, $this->dataFormatFormParams($data), $headers);
    }

    /**
     * Delete User By Id
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteUserById(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('users/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Sign in
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function signin(array $data = [], array $headers = []): array
    {
        return $this->post('signin', $this->dataFormatFormParams($data), $headers);
    }

    /**
     * Passwordless
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function passwordless(array $data = [], array $headers = []): array
    {
        return $this->post('passwordless', $this->dataFormatFormParams($data), $headers);
    }


}


