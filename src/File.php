<?php


namespace Int\Services\Client;


class File extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.file/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'file';


    /**
     * Create File
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createFile(array $data = [], array $headers = []): array
    {
        return $this->post('files' , $data, $headers);
    }
}


