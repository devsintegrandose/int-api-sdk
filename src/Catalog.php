<?php


namespace Int\Services\Client;


class Catalog extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.catalog/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'catalog';



    /**
     * Show Utem
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showItem(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('items/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Create service VTEX
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createServiceVtex(array $data = [], array $headers = []): array
    {
        return $this->post('items/services', $this->dataFormatJson($data), $headers);
    }

    /**
     * List services VTEX
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listServicesVtex(array $data = [], array $headers = []): array
    {
        return $this->get('items/services', $this->dataFormatJson($data), $headers);
    }

    /**
     * Show service VTEX
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showServiceVtex(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('items/services/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Delete service VTEX
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteServiceVtex(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('items/services/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Update service VTEX
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateServiceVtex(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('items/services/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Create theme VTEX
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createThemeVtex(array $data = [], array $headers = []): array
    {
        return $this->post('items/themes', $this->dataFormatJson($data), $headers);
    }

    /**
     * List themes VTEX
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listThemesVtex(array $data = [], array $headers = []): array
    {
        return $this->get('items/themes', $this->dataFormatJson($data), $headers);
    }

    /**
     * Show theme VTEX
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showThemeVtex(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Delete theme VTEX
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteThemeVtex(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Update theme VTEX
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateThemeVtex(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Update partner
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updatePartner(array $data = [], array $headers = []): array
    {
        return $this->patch('items/partner', $this->dataFormatJson($data), $headers);
    }

    /**
     * Update Rating
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateRating(string $id, array $data = [], array $headers = [])
    {
        return $this->patch('items/' . $id . '/rating', $this->dataFormatJson($data), $headers);
    }


    /**
     * Status Store
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function statusStore(string $id, array $data = [], array $headers = [])
    {
        return $this->patch('items/' . $id . '/status-store', $this->dataFormatJson($data), $headers);
    }


    /**
     * Status
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function UpdateStatus(string $id, array $data = [], array $headers = [])
    {
        return $this->patch('items/' . $id . '/status', $this->dataFormatJson($data), $headers);
    }


     /**
     * Status Theme LI
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function UpdateStatusThemeLojaIntegrada(string $id, array $data = [], array $headers = [])
    {
        return $this->patch('items/' . $id . '/status-theme-loja-integrada', $this->dataFormatJson($data), $headers);
    }


    /**
     * List Segments
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listSegments(array $data = [], array $headers = []): array
    {
        return $this->get('segments', $this->dataFormatJson($data), $headers);
    }


    /**
     * Create Segment
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createSegment(array $data = [], array $headers = []): array
    {
        return $this->post('segments', $this->dataFormatJson($data), $headers);

    }

    /**
     * Show Segment
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showSegment(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('segments/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Update Segment
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateSegment(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('segments/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Delete Segment
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteSegment(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('segments/' . $id, $this->dataFormatJson($data), $headers);
    }



    /**
     * List Banners
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listBanners(array $data = [], array $headers = []): array
    {
        return $this->get('banners', $this->dataFormatJson($data), $headers);
    }


    /**
     * Create Banner
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createBanner(array $data = [], array $headers = []): array
    {
        return $this->post('banners', $this->dataFormatJson($data), $headers);

    }

    /**
     * Show Banner
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showBanner(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('banners/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Update Banner
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateBanner(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('banners/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Delete Banner
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteBanner(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('banners/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Create theme Loja Integrada
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createThemeLojaIntegrada(array $data = [], array $headers = []): array
    {
        return $this->post('items/themes', $this->dataFormatJson($data), $headers);
    }

    /**
     * List themes Loja Integrada
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listThemesLojaIntegrada(array $data = [], array $headers = []): array
    {
        $data = array_merge($data, ['filter' => ['type_item'=> 'theme-loja-integrada']]);
        return $this->get('items/themes', $this->dataFormatJson($data), $headers);
    }


    /**
     * Show theme Loja Integrada
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showThemeLojaIntegrada(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Delete theme Loja Integrada
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteThemeLojaIntegrada(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Update theme Loja Integrada
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateThemeLojaIntegrada(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Update Version Theme
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateVersionThemeLojaIntegrada(string $id, array $data = [], array $headers = []): array
    {
        return $this->post('items/themes/' . $id . '/versions', $this->dataFormatJson($data), $headers);
    }

      /**
     * Update status Version Theme
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateStatusVersionThemeLojaIntegrada(string $idTheme, string $idVersion, array $data = [], array $headers = []): array
    {
        return $this->put('items/themes/' . $idTheme . '/versions/' . $idVersion, $this->dataFormatJson($data), $headers);
    }


    #################################################
    ##      Apps Loja Integrada                    ##
    ##      app-loja-integrada                     ##
    #################################################


    /**
     * Create app Loja Integrada
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createAppLojaIntegrada(array $data = [], array $headers = []): array
    {
        return $this->post('items/apps', $this->dataFormatJson($data), $headers);
    }

    /**
     * List app Loja Integrada
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listAppsLojaIntegrada(array $data = [], array $headers = []): array
    {
        return $this->get('items/apps', $this->dataFormatJson($data), $headers);
    }


    /**
     * Show app Loja Integrada
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showAppLojaIntegrada(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('items/apps/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Delete app Loja Integrada
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteAppLojaIntegrada(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('items/apps/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Update app Loja Integrada
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateAppLojaIntegrada(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('items/apps/' . $id, $this->dataFormatJson($data), $headers);
    }

    public function getStatsServices(array $data = [], array $headers = []): array
    {
        return $this->get('items/services/stats', $this->dataFormatJson($data), $headers);
    }







    /**
     * List Recommendeds
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listRecommendeds(array $data = [], array $headers = []): array
    {
        $filter = ['filter' => [
            'is_recommended' => true
        ]];

        $data = array_merge_recursive($data, $filter);

        return $this->get('items/recommendeds', $this->dataFormatJson($data), $headers);
    }

    /**
     * Create Recommended
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createRecommended(array $data = [], array $headers = []): array
    {
        return $this->post('items/recommendeds', $this->dataFormatJson($data), $headers);
    }

    /**
     * Delete Recommended
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteRecommended(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('items/recommendeds/' . $id, $this->dataFormatJson($data), $headers);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
     /**
     * Create theme XTECH
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createThemeXtech(array $data = [], array $headers = []): array
    {
        return $this->post('items/themes', $this->dataFormatJson($data), $headers);
    }

    /**
     * List themes XTECH
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listThemesXtech(array $data = [], array $headers = []): array
    {
        $data = array_merge($data, ['filter' => ['type_item'=> 'theme-xtech']]);
        return $this->get('items/themes', $this->dataFormatJson($data), $headers);
    }

    /**
     * Show theme XTECH
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showThemeXtech(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Delete theme XTECH
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteThemeXtech(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Update theme XTECH
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateThemeXtech(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('items/themes/' . $id, $this->dataFormatJson($data), $headers);
    }
}


