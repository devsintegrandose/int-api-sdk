<?php


namespace Int\Services\Client;


class Review extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.review/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'review';


    /**
     * Reviews by product
     *
     * @param $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function reviewsByProduct($id,array $data = [], array $headers = []): array
    {
        return $this->get('reviews/product/'.$id, $this->dataFormatFormParams($data), $headers);
    }


    /**
     * Reviews by customer
     *
     * @param $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function reviewsByCustomer($id,array $data = [], array $headers = []): array
    {
        return $this->get('reviews/customer/'.$id, $this->dataFormatFormParams($data), $headers);
    }



    /**
     * Update Product
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateProduct( array $data = [], array $headers = []): array
    {
        return $this->patch('reviews/product',  $this->dataFormatJson($data), $headers);
    }


    /**
     * Create Review
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createReview(array $data = [], array $headers = []): array
    {
        return $this->post('reviews', $this->dataFormatJson($data), $headers);
    }

}


