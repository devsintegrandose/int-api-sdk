<?php


namespace Int\Services\Client;


class Customer extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.customer/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'customer';


    /**
     * Show Customer
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showCustomer(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('customers/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Show Customer by e-mail
     *
     * @param string $email
     * @param array $data
     * @param array $headers
     * @return array\
     */
    public function showCustomerByEmail(string $email, array $data = [], array $headers = []): array
    {
        return $this->get('customers/email/' . $email, $this->dataFormatJson($data), $headers);
    }

    /**
     * Create Customer
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createCustomer(array $data = [], array $headers = []): array
    {
        return $this->post('customers/', $this->dataFormatJson($data), $headers);
    }


    /**
     * List customer
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listCustomers(array $data = [], array $headers = []): array
    {
        return $this->get('customers', $this->dataFormatJson($data), $headers);
    }

    /**
     * Add BookMark
     *
     * @param strin $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function addBookmark(string $id, array $data = [], array $headers = []): array
    {
        return $this->post('customers/' . $id . '/bookmark', $this->dataFormatJson($data), $headers);
    }

    /**
     * Delete BookMark
     *
     * @param strin $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function deleteBookmark(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('customers/' . $id . '/bookmark/'. $data['product_id'], $this->dataFormatJson($data), $headers);
    }

}


