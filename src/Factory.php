<?php


namespace Int\Services\Client;


use GuzzleHttp\Client;

class Factory extends ClientAbstract
{

    /**
     * Make service client
     *
     * @param string $service
     * @param string|null $endpoint
     * @return ClientAbstract
     * @throws \Exception
     */
    public static function make(string $service, string $endpoint = null): ClientAbstract
    {

        $nameClass = 'Int\\Services\\Client\\' . ucfirst($service);

        if (!class_exists($nameClass)) {
            throw new \Exception('Client does not exists');
        }

        $client = new $nameClass(new Client());

        if ($endpoint) {
            $client->setEndpoint($endpoint);
        }

        return $client;
    }
}


