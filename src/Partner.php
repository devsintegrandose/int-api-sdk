<?php


namespace Int\Services\Client;


class Partner extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.partner/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'partner';


    /**
     * Show Partner
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showPartner(string $id,array $data = [], array $headers = []): array
    {
        return $this->get('partners/' . $id , $this->dataFormatJson($data), $headers);
    }



    /**
     * List partners
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listPartners(array $data = [], array $headers = []): array
    {
        return $this->get('partners', $this->dataFormatJson($data), $headers);
    }

    /**
     * Create Partner
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createPartner(array $data = [], array $headers = []): array
    {
        return $this->post('partners/', $this->dataFormatJson($data), $headers);
    }

    /**
     * Update Status
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateStatus(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('partners/'.$id.'/status', $this->dataFormatJson($data), $headers);
    }


     /**
     * Get Stats
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function getStats(array $data = [], array $headers = []): array
    {
        return $this->get('partners/stats', $this->dataFormatJson($data), $headers);
    }


    /**
     * List admins
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listAdminsByPartnerId(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('private/partners/'.$id.'/admins', $this->dataFormatJson($data), $headers);
    }
}


