<?php


namespace Int\Services\Client;


class Question extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.question/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'question';


    /**
     * Create Question
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createQuestion(array $data = [], array $headers = []): array
    {
        return $this->post('questions', $this->dataFormatJson($data), $headers);
    }


    /**
     * Create Response
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createResponse(array $data = [], array $headers = []): array
    {
        return $this->post('questions/answer', $this->dataFormatJson($data), $headers);
    }

    /**
     * List questions
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listQuestions(array $data = [], array $headers = []): array
    {
        return $this->get('questions/response', $this->dataFormatJson($data), $headers);
    }

    /**
     * List questions by partner
     *
     * @param $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listQuestionsByPartner($id, array $data = [], array $headers = []): array
    {
        return $this->get('questions/partner/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Show question
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showQuestion(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('questions/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Stats by partner
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function statsByPartner(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('questions/stats/partner/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Update Product
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateProduct( array $data = [], array $headers = []): array
    {
        return $this->patch('questions/product',  $this->dataFormatJson($data), $headers);
    }

    /**
     * Questions by product
     *
     * @param $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function questionsByProduct($id,array $data = [], array $headers = []): array
    {
        return $this->get('questions/product/'.$id, $this->dataFormatFormParams($data), $headers);
    }

}


