<?php


namespace Int\Services\Client;


class Notification extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.notification/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'notification';


    /**
     * Send e-mail
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function sendEmail(array $data = [], array $headers = []): array
    {
        return $this->post('email', $this->dataFormatFormParams($data), $headers);
    }

    /**
     * Send sms
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function sendSms(array $data = [], array $headers = []): array
    {
        return $this->post('sms', $this->dataFormatFormParams($data), $headers);
    }


}


