<?php


namespace Int\Services\Client;

class Lead extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.lead/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'lead';

    const STATUS_PAYMENT_PENDING_PAYMENT = 'PENDING_PAYMENT';
    const STATUS_PAYMENT_PAID = 'PAID';


    /**
     * Reviews by product
     *
     * @param $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function leadsByProduct($id,array $data = [], array $headers = []): array
    {
        return $this->get('leads/product/'.$id, $this->dataFormatFormParams($data), $headers);
    }

    public function showLead(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('leads/' . $id, $this->dataFormatJson($data), $headers);
    }

    public function updateLead(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('leads/' . $id, $this->dataFormatJson($data), $headers);
    }


    public function listLeads( array $data = [], array $headers = []): array
    {
        return $this->get('leads', $this->dataFormatJson($data), $headers);
    }

    public function listLeadsByPartner($id, array $data = [], array $headers = []): array
    {
        return $this->get('leads/partner/' . $id, $this->dataFormatJson($data), $headers);
    }


    public function statsByPartner($id, array $data = [], array $headers = []): array
    {
        return $this->get('leads/partner/' . $id . '/stats', $this->dataFormatJson($data), $headers);
    }

    public function markAsPaid(array $data = [], array $headers = []): array
    {
        return $this->post('leads/markas/paid', $this->dataFormatJson($data), $headers);
    }

    /**
     * Reviews by customer
     *
     * @param $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function leadsByCustomer($id,array $data = [], array $headers = []): array
    {
        return $this->get('leads/customer/'.$id, $this->dataFormatFormParams($data), $headers);
    }



    /**
     * Update Product
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateProduct( array $data = [], array $headers = []): array
    {
        return $this->patch('leads/product',  $this->dataFormatJson($data), $headers);
    }


    /**
     * Create Review
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createLead(array $data = [], array $headers = []): array
    {
        return $this->post('leads', $this->dataFormatJson($data), $headers);
    }

}


