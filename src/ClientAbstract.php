<?php

namespace Int\Services\Client;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Int\Services\Client\Exception\ServiceClientException;

class ClientAbstract
{

    /**
     * Guzzle
     *
     * @var \GuzzleHttp\Client
     */
    private $httpClient;


    /**
     * Last Response
     *
     * @var \GuzzleHttp\Psr7\Response
     */
    private $lastResponse;


    /**
     * ClientAbstract constructor.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(\GuzzleHttp\Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }


    public function getLastResponse(): Response
    {
        return $this->lastResponse;
    }


    /**
     * Set Endpoint
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Get Endpoint
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * Define url to request
     * @param string $uri
     * @return string
     */
    protected function getEndpointUrl(string $uri): string
    {
        if (function_exists('env') && env('APP_ENV') == 'staging') {
            return str_replace('http://','http://staging-',$this->endpoint) . '/' . $uri;
        }

        return $this->endpoint . '/' . $uri;
    }


    /**
     * Patch
     *
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return array
     */
    protected function patch(string $uri, array $data = [], array $headers = [])
    {
        return $this->send('PATCH', $uri, $data, $headers);
    }

    /**
     * Get
     *
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return array
     */
    protected function get(string $uri, array $data = [], array $headers = [])
    {
        return $this->send('GET', $uri, $data, $headers);
    }

    /**
     * Put
     *
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return array
     */
    protected function put(string $uri, array $data = [], array $headers = [])
    {
        return $this->send('PUT', $uri, $data, $headers);
    }

    /**
     * Post
     *
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return array
     */
    protected function post(string $uri, array $data = [], array $headers = [])
    {
        return $this->send('POST', $uri, $data, $headers);
    }


    /**
     * Delete
     *
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return array
     */
    protected function delete(string $uri, array $data = [], array $headers = [])
    {
        return $this->send('DELETE', $uri, $data, $headers);
    }


    /**
     * Send
     *
     * @param string $httpVerb
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return array
     */
    private function send(string $httpVerb, string $uri, array $data, array $headers): array
    {
        try {
            $request = new Request($httpVerb, $this->getEndpointUrl($uri));
            $data['headers'] = $headers;

            $this->lastResponse = $this->httpClient->send($request, $data);

            return $this->decodeJson($this->lastResponse);
        } catch (ClientException $e) {

            $exception = new ServiceClientException(
                $e->getMessage(),
                $e->getRequest(),
                $e->getResponse(),
                null,
                $e->getHandlerContext()
            );

            $exception->setService($this->service);

            throw $exception;
        }
    }

    /**
     * Decode response to array
     *
     * @param Response $response
     * @return array
     */
    private function decodeJson(Response $response): array
    {
        return json_decode($response->getBody()->getContents(), true) ?? [];
    }

    /**
     * Data format to json
     *
     * @param array $data
     * @return array
     */
    protected function dataFormatJson(array $data): array
    {
        return ['json' => $data];
    }

    /**
     * Data format to form params
     *
     * @param array $data
     * @return array
     */
    protected function dataFormatFormParams(array $data): array
    {
        return ['form_params' => $data];
    }
}