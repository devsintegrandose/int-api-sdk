<?php


namespace Int\Services\Client;


class Billing extends ClientAbstract
{
    const SUBSCRIPTION_STATUS_PENDING_PAYMENT = 'PENDING_PAYMENT';
    const SUBSCRIPTION_STATUS_CANCELED = 'CANCELED';
    const SUBSCRIPTION_STATUS_ACTIVE = 'ACTIVE';

    const INVOICE_STATUS_FAILED = 'FAILED';
    const INVOICE_STATUS_CANCELED = 'CANCELED';
    const INVOICE_STATUS_PAYED = 'PAYED';
    const INVOICE_STATUS_PENDING_PAYMENT = 'PENDING_PAYMENT';
    const INVOICE_STATUS_REFUNDED = 'REFUNDED';

    const PAYMENT_METHOD_CREDIT_CARD = 'credit_card';
    const PAYMENT_METHOD_BOLETO = 'boleto';

    const PERIOD_MONTHLY = 'monthly';
    const PERIOD_YEARLY = 'yearly';
    const PERIOD_MONTHLY_YEARLY = 'monthly_yearly';

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.billing/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'billing';


    /**
     * Create Subscription
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createSubscription(array $data = [], array $headers = []): array
    {
        return $this->post('billing/subscription', $this->dataFormatJson($data), $headers);
    }


    /**
     * Create Subscription
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateSubscription(array $data = [], array $headers = []): array
    {
        return $this->put('billing/subscription', $this->dataFormatJson($data), $headers);
    }


    /**
     * Cancel Subscription
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function cancelSubscription(array $data = [], array $headers = []): array
    {
        return $this->delete('billing/subscription', $this->dataFormatJson($data), $headers);
    }



    /**
     * List questions by partner
     *
     * @param $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function subscriptionByPartner($id, array $data = [], array $headers = []): array
    {
        return $this->get('billing/subscription/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * List Plans
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listPlans( array $data = [], array $headers = []): array
    {
        return $this->get('billing/plans' , $this->dataFormatJson($data), $headers);
    }


    public function showPlan($id, array $data = [], array $headers = []): array
    {
        return $this->get('billing/plans/'.$id , $this->dataFormatJson($data), $headers);
    }

    public function createPlan(array $data = [], array $headers = []): array
    {
        return $this->post('billing/plans', $this->dataFormatFormParams($data), $headers);
    }

    public function updatePlanById(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('billing/plans/' . $id, $this->dataFormatFormParams($data), $headers);
    }

    public function deletePlanById(string $id, array $data = [], array $headers = []): array
    {
        return $this->delete('billing/plans/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * List Invoices
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listInvoices( array $data = [], array $headers = []): array
    {
        return $this->get('billing/invoices' , $this->dataFormatJson($data), $headers);
    }

    
    public function showInvoice($id, array $data = [], array $headers = []): array
    {
        return $this->get('billing/invoices/'.$id , $this->dataFormatJson($data), $headers);
    }

    public function listInvoicesByPartner($id, array $data = [], array $headers = []): array
    {
        return $this->get('billing/invoices/partner/'.$id , $this->dataFormatJson($data), $headers);
    }

    public function cancelOrRefundInvoice($id, array $data = [], array $headers = []): array
    {
        return $this->delete('billing/invoices/'.$id , $this->dataFormatJson($data), $headers);
    }

}


