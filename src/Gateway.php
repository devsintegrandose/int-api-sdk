<?php


namespace Int\Services\Client;


class Gateway extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.gateway/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'gateway';


    public function flushCache(array $data = [], array $headers = []): array
    {
        return $this->get('clear-cache/vtex-store-integrando-se', $this->dataFormatFormParams($data), $headers);
    }

}


