<?php


namespace Int\Services\Client;


class Checkout extends ClientAbstract
{

    /**
     * Endpoint
     *
     * @var string
     */
    protected $endpoint = "http://api.checkout/v1";

    /**
     * service
     *
     * @var string
     */
    protected $service = 'checkout';


    /**
     * Stats
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function stats(array $data = [], array $headers = []): array
    {
        return $this->get('orders/stats', $this->dataFormatJson($data), $headers);
    }


    /**
     * Stats
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function statsGlobal(array $data = [], array $headers = []): array
    {
        return $this->get('orders/stats-global', $this->dataFormatJson($data), $headers);
    }


    /**
     * Show order
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showOrder(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('orders/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * List orders
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listOrders(array $data = [], array $headers = []): array
    {
        return $this->get('orders', $this->dataFormatJson($data), $headers);
    }

    /**
     * List cupons
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listCoupons(array $data = [], array $headers = []): array
    {
        return $this->get('coupons', $this->dataFormatJson($data), $headers);
    }

    /**
     * Create coupon
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function createCoupon(array $data = [], array $headers = []): array
    {
        return $this->post('coupons', $this->dataFormatJson($data), $headers);
    }

    /**
     * Show coupon
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showCoupon(string $id,array $data = [], array $headers = []): array
    {
        return $this->get('coupons/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Show coupon
     *
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function updateCoupon(string $id,array $data = [], array $headers = []): array
    {
        return $this->put('coupons/' . $id, $this->dataFormatJson($data), $headers);
    }


    public function deleteCoupon(string $id,array $data = [], array $headers = []): array
    {
        return $this->delete('coupons/' . $id, $this->dataFormatJson($data), $headers);
    }


    /**
     * Show Subscription
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function showSubscription(string $id, array $data = [], array $headers = []): array
    {
        return $this->get('subscriptions/' . $id, $this->dataFormatJson($data), $headers);
    }

    /**
     * Cancel Subscription
     *
     * @param string $id
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function cancelSubscription(string $id, array $data = [], array $headers = []): array
    {
        return $this->put('subscriptions/' . $id . '/cancel', $this->dataFormatJson($data), $headers);
    }

    /**
     * List Subscriptions
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function listSubscriptions(array $data = [], array $headers = []): array
    {
        return $this->get('subscriptions', $this->dataFormatJson($data), $headers);
    }
}