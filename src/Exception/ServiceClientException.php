<?php

namespace Int\Services\Client\Exception;

use GuzzleHttp\Exception\ClientException;


class ServiceClientException extends ClientException
{

    private $service;

    public function setService($service) {
        $this->service = $service;
    }


    public function getErros()
    {
        $dataJson = json_decode($this->getResponse()->getBody()->getContents(), true);
        $dataJson['service_client_error'] = $this->service;
        return $dataJson;
    }
}